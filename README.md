# curso-docker

- Hicimos un git clone para bajar las cosas de nuestra rama
- Agregamos los archivos index y dockerfile
- hicimos un git add .
- con un git status, me muestra el estado de los ficheros
- con Git commit -m "comentario" , se hace el commit de los cambios con un comentario. 
- Git push, sube los cambios a gitlab.
---------------------
- Levantamos el docker de Jenkins y de docker in docker
	docker container run --rm --detach --privileged --network jenkins --network-alias docker --env DOCKER_TLS_CERTDIR=/certs --volume jenkins-docker-certs:/certs/client --volume jenkins-data:/var/jenkins_home --publish 2376:2376 docker:dind
	docker container run --rm --detach --network jenkins --env DOCKER_HOST=tcp://docker:2376 --env DOCKER_CERT_PATH=/certs/client --env DOCKER_TLS_VERIFY=1 -p 8080:8080 -p 50000:50000 --volume jenkins_prueba:/var/jenkins_home --volume jenkins-docker-certs:/certs/client:ro jenkinsci/blueocean
- Configuramos Jenkins
- Agregamos los siguientes plugins
	Active Choices
	Extended Choice Parameter
	Pipeline Utility Steps
	Pipeline: Groovy HTTP
	Gitlab API plugin
	Gitlab Hook plugin
	SSH Agent
	Build Monitor View
	Gitlab
- Creamos un proyecto del tipo pipeline y en el script le pusimos:
	pipeline {
		agent any
		stages {
			stage('Hello') {
				steps {
					echo 'Hello World'
				}
			}
			stage('Check directory') {
				steps {
					sh 'pwd'
				}
			}
			stage('Check content') {
				steps {
					sh 'ls -la'
				}
			}
		}
	}
- Creamos un proyecto del tipo Folder, y pusimos el proyecto en ese folder
- Creamos un nuevo proyecto del tipo pipeline, del tipo SCM
- Vinculamos al Gitlab
- creamos un archivo Jenkinsfile con el mismo contenido que en el pipeline anterior.
- Subimos el jenkinsfile al gitlab.
